private
  def find_cart
    @cart = (session[:cart] || Cart.new)
    # if there's no cart in the session add a new one
  end

  def add_to_cart
    @cart = find_cart
    product = Product.find(params[:id])
    @cart.add_product(product)
  end
end

