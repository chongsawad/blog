ActionController::Routing::Routes.draw do |map|

# map.login "login", :controller => "user_sessions", :action => "new"
# map.logout "logout", :controller => "user_sessions", :action => "destroy"

  map.devise_for :users, :path_names => { :sign_in => 'login', :sign_out => 'logout', :sign_up => 'register', :password => 'secret', :confirmation => 'verification', :unlock => 'unblock' }

  map.resources :posts, :has_many => :comments

  map.resources :user_sessions

  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'

# map.root :controller => "user_sessions", :action => "new"
  map.root :controller => "posts"


end

